{
  "PageType": 1,
  "ColumnCount": 63,
  "RowCount": 25,
  "CustomNames": [
    {
      "Name": "菜单显示",
      "Formula": "'前台菜单导航-PC (菜单不绑定数据表)'!$BK$4"
    },
    {
      "Name": "选中菜单的ID",
      "Formula": "'前台菜单导航-PC (菜单不绑定数据表)'!$BK$5"
    },
    {
      "Name": "Menu",
      "Formula": "'前台菜单导航-PC (菜单不绑定数据表)'!$A$4"
    }
  ]
}