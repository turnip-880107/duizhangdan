//这个文件里面的函数相当于活字格的"页面加载完成命令"
//由于"登录"页面没有"页面加载完成命令",所以只能出此下策

const targetNode_observerDemo = document.querySelector("head"); // 监听<head>元素

// 定义检查函数
function checkForDiv_observerDemo() {
    
    if (document.title === "登录") {
        const divElement_observerDemo = document.querySelector('div[fgc-gui-bgname="背景图1"] > div');//这一句和下面这个if貌似是多余的,感觉标题变成登录的时候背景图早已加载好了,反正
        if (divElement_observerDemo) {
            //console.log('登录页面加载已完成');
            customizeBackgroundAndDotColor('rgb(5, 18, 37)', '0.094,0.564,1.0,1.0');//把背景图片替换成3D显示
            // 如果你只想只到首次出现，你可以在此处断开观察
            // observerInstance_observerDemo.disconnect(); 
        }
    }
}

// MutationObserver 的回调函数
const callback_observerDemo = function (mutationsList, observerInstance) {
    for (let mutation of mutationsList) {
        if (mutation.type === 'childList' && mutation.target.nodeName === 'TITLE') { // 确保是<title>元素的变化
            checkForDiv_observerDemo();
        }
    }
};

const config_observerDemo = { childList: true, subtree: true };
const observerInstance_observerDemo = new MutationObserver(callback_observerDemo);
observerInstance_observerDemo.observe(targetNode_observerDemo, config_observerDemo);
